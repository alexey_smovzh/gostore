package settings

const LISTEN = "127.0.0.1"
const PORT = "8080"
const DATA = "./data"
const DATABASE = DATA + "/database.db"
const SCHEMA = "./schema.sql"
const MEDIA = DATA + "/media"
const MAX_SIZE = 10 * 1024 * 1024 // max image size to upload is 10Mb
const FORMAT = "image/png"
