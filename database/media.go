package database

import (
	"database/sql"
	"errors"
	"fmt"
)

type Media struct {
	Name string
}

func GetMedia(db *sql.DB, id string) ([]*Media, error) {
	if db == nil {
		return nil, errors.New("database not found")
	}

	s := fmt.Sprintf("SELECT name FROM media WHERE type = 'p' AND product_id = '%s'", id)
	rows, err := db.Query(s)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	result := []*Media{}
	for rows.Next() {
		media := &Media{}

		err := rows.Scan(&media.Name)
		if err != nil {
			return nil, err
		}

		result = append(result, media)
	}

	return result, nil
}

func InsertMedia(db *sql.DB, id string, name string) error {
	s := fmt.Sprintf("INSERT INTO media(product_id, name, type) VALUES('%s', '%s', 'p')", id, name)
	return DbQuery(db, s)
}

func DeleteMedia(db *sql.DB, id string) error {
	s := fmt.Sprintf("DELETE FROM media WHERE product_id = '%s'", id)
	return DbQuery(db, s)
}
