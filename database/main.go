package database

import (
	"database/sql"
)

var DB *sql.DB
var err error

func init() {
	InitDatabase()

	DB, err = DbConnect()
	if err != nil {
		println(err)
	}

}
