package database

import (
	"database/sql"
	"errors"
	"fmt"
)

type Composition struct {
	Id   int32
	Name string
	Qty  string
}

func GetComposition(db *sql.DB, id string) ([]*Composition, error) {
	if db == nil {
		return nil, errors.New("database not found")
	}

	s := fmt.Sprintf(`SELECT composition.id, ingredient.name, composition.qty
					  FROM ingredient, composition
					  WHERE ingredient.id = composition.ingredient_id
					  AND composition.product_id = '%s'`, id)

	rows, err := db.Query(s)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	result := []*Composition{}
	for rows.Next() {
		composition := &Composition{}

		err := rows.Scan(&composition.Id, &composition.Name, &composition.Qty)
		if err != nil {
			return nil, err
		}

		result = append(result, composition)
	}

	return result, nil
}

func InsertComposition(db *sql.DB, product_id string, ingredient_id string, qty string) error {
	s := fmt.Sprintf("INSERT INTO composition(product_id, ingredient_id, qty) VALUES('%s', '%s', '%s')", product_id, ingredient_id, qty)
	return DbQuery(db, s)
}

func DeleteComposition(db *sql.DB, id string) error {
	s := fmt.Sprintf("DELETE FROM composition WHERE product_id = '%s'", id)
	return DbQuery(db, s)
}
