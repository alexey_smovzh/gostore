package database

import (
	"database/sql"
	"errors"
	"fmt"
)

type Unit struct {
	Id   int32
	Name string
}

func GetAllUnits(db *sql.DB) ([]*Unit, error) {
	if db == nil {
		return nil, errors.New("database not found")
	}

	rows, err := db.Query("SELECT id, name FROM unit")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	result := []*Unit{}
	for rows.Next() {
		unit := &Unit{}

		err := rows.Scan(&unit.Id, &unit.Name)
		if err != nil {
			return nil, err
		}

		result = append(result, unit)
	}

	return result, nil
}

func InsertUnit(db *sql.DB, unit string) error {
	s := fmt.Sprintf("INSERT INTO unit(name) VALUES('%s')", unit)
	return DbQuery(db, s)
}

func DeleteUnit(db *sql.DB, id string) error {
	s := fmt.Sprintf("DELETE FROM unit WHERE id = '%s'", id)
	return DbQuery(db, s)
}
