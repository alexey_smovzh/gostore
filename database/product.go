package database

import (
	"database/sql"
	"errors"
	"fmt"
)

// Products list
type Product struct {
	Id      int32
	Name    string
	Qty     int16
	Price   int32
	Unit_id int32
	Unit    string
}

// All products list for Store Client App
type ProductStore struct {
	Id      int32
	Name    string
	Qty     int16
	Price   int32
	Unit    string
	Laconic string
	Image   string
}

// Single product description
type Description struct {
	Id      int32
	Name    string
	Qty     int16
	Price   int32
	Unit    string
	Laconic string
	Full    string
}

func GetAllProducts(db *sql.DB) ([]*Product, error) {
	if db == nil {
		return nil, errors.New("database not found")
	}

	rows, err := DB.Query(`SELECT product.id, product.name, product.qty, product.price, unit.id as unit_id, unit.name as unit
						   FROM product, unit
						   WHERE product.unit_id = unit.id`)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	result := []*Product{}
	for rows.Next() {
		product := &Product{}

		err := rows.Scan(&product.Id, &product.Name, &product.Qty, &product.Price, &product.Unit_id, &product.Unit)
		if err != nil {
			return nil, err
		}

		result = append(result, product)
	}

	return result, nil
}

func GetStoreAllProducts(db *sql.DB) ([]*ProductStore, error) {
	if db == nil {
		return nil, errors.New("database not found")
	}

	rows, err := DB.Query(`SELECT product.id, product.name, product.qty, product.price, unit.name as unit, product.laconic, media.name as image
						   FROM product, unit, media
						   WHERE product.id = media.product_id
						   AND product.price > 0
						   AND product.unit_id = unit.id
						   AND media.id = (SELECT MIN(id)
						   				   FROM media
						   				   WHERE media.product_id = product.id)`)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	result := []*ProductStore{}
	for rows.Next() {
		product := &ProductStore{}

		err := rows.Scan(&product.Id, &product.Name, &product.Qty, &product.Price, &product.Unit, &product.Laconic, &product.Image)
		if err != nil {
			return nil, err
		}

		result = append(result, product)
	}

	return result, nil
}

func GetProduct(db *sql.DB, id string) ([]*Product, error) {
	if db == nil {
		return nil, errors.New("database not found")
	}

	s := fmt.Sprintf(`SELECT product.id, product.name, product.qty, product.price, unit.id as unit_id, unit.name as unit
					  FROM product, unit
					  WHERE product.unit_id = unit.id
					  AND product.id = '%s'`, id)

	product := &Product{}
	rows := db.QueryRow(s)
	err := rows.Scan(&product.Id, &product.Name, &product.Qty, &product.Price, &product.Unit_id, &product.Unit)
	if err != nil {
		return nil, err
	}

	result := []*Product{}
	result = append(result, product)

	return result, nil
}

func UpdateProduct(db *sql.DB, product string, id string, qty string, price string, unit_id string) error {
	s := fmt.Sprintf("UPDATE product SET name='%s', qty='%s', price='%s', unit_id='%s' WHERE id='%s'", product, qty, price, unit_id, id)
	return DbQuery(db, s)
}

func UpdateProductQty(db *sql.DB, product string, qty int) error {
	s := fmt.Sprintf(`UPDATE product
					  SET qty=(SELECT qty - %d FROM product WHERE name = '%s')
					  WHERE name = '%s'`, qty, product, product)
	return DbQuery(db, s)
}

func InsertProduct(db *sql.DB, product string, qty string, price string, unit_id string) error {
	s := fmt.Sprintf("INSERT INTO product(name, qty, price, unit_id) VALUES('%s', '%s', '%s', '%s')", product, qty, price, unit_id)
	return DbQuery(db, s)
}

func DeleteProduct(db *sql.DB, id string) error {
	s := fmt.Sprintf("DELETE FROM product WHERE id = '%s'", id)
	return DbQuery(db, s)
}

func GetDescription(db *sql.DB, id string) ([]*Description, error) {
	if db == nil {
		return nil, errors.New("database not found")
	}

	s := fmt.Sprintf(`SELECT product.id, product.name, product.qty, product.price, unit.name as unit, product.laconic, product.full
					  FROM product, unit
					  WHERE product.unit_id = unit.id
					  AND product.id = '%s'`, id)

	description := &Description{}
	rows := db.QueryRow(s)
	err := rows.Scan(&description.Id, &description.Name, &description.Qty, &description.Price, &description.Unit, &description.Laconic, &description.Full)
	if err != nil {
		return nil, err
	}

	result := []*Description{}
	result = append(result, description)

	return result, nil
}

func UpdateDescription(db *sql.DB, laconic string, full string, id string) error {
	s := fmt.Sprintf("UPDATE product SET laconic='%s', full='%s' WHERE id='%s'", laconic, full, id)
	return DbQuery(db, s)
}
