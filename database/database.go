package database

import (
	"database/sql"
	"gostore/settings"
	"io/ioutil"
	"os"

	_ "github.com/mattn/go-sqlite3"
)

// Create database file if not exists
func createDbFile() error {
	_, err := os.Stat(settings.DATA)
	if os.IsNotExist(err) {
		err = os.MkdirAll(settings.DATA, 0700)
		if err != nil {
			return err
		}

		err = os.WriteFile(settings.DATABASE, nil, 0644)
		if err != nil {
			return err
		}
	}
	return nil
}

// Load DB Schema
func loadDbSchema(db *sql.DB) error {
	schema, err := ioutil.ReadFile(settings.SCHEMA)
	if err != nil {
		return err
	}
	sql := string(schema)
	_, err = db.Exec(sql)
	if err != nil {
		return err
	}
	return nil
}

var db *sql.DB

// Open database connection
func DbConnect() (*sql.DB, error) {
	if db == nil {
		db, err = sql.Open("sqlite3", settings.DATABASE+"?_foreign_keys=on")
		if err != nil {
			return nil, err
		}
		err = db.Ping()
		if err != nil {
			return nil, err
		}
	}
	return db, nil
}

func DbClose(db *sql.DB) {
	db.Close()
}

// Create database and load SQL schema if it not exists
// Create database connection pool
func InitDatabase() error {
	_, err := os.Stat(settings.DATABASE)
	if os.IsNotExist(err) {
		err = createDbFile()
		if err != nil {
			return err
		}

		db, err := DbConnect()
		if err != nil {
			return err
		}

		loadDbSchema(db)
		if err != nil {
			return err
		}
	}
	return nil
}
