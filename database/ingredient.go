package database

import (
	"database/sql"
	"errors"
	"fmt"
)

type Ingredient struct {
	Id   int32
	Name string
}

func GetAllIngredients(db *sql.DB) ([]*Ingredient, error) {
	if db == nil {
		return nil, errors.New("database not found")
	}

	rows, err := db.Query("SELECT id, name FROM ingredient")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	result := []*Ingredient{}
	for rows.Next() {
		ingredient := &Ingredient{}

		err := rows.Scan(&ingredient.Id, &ingredient.Name)
		if err != nil {
			return nil, err
		}

		result = append(result, ingredient)
	}

	return result, nil
}

func GetIngredient(db *sql.DB, id string) ([]*Ingredient, error) {
	if db == nil {
		return nil, errors.New("database not found")
	}

	ingredient := &Ingredient{}
	rows := db.QueryRow("SELECT id, name FROM ingredient WHERE id = ?", id)
	err := rows.Scan(&ingredient.Id, &ingredient.Name)
	if err != nil {
		return nil, err
	}

	result := []*Ingredient{}
	result = append(result, ingredient)

	return result, nil
}

func UpdateIngredient(db *sql.DB, ingredient string, id string) error {
	s := fmt.Sprintf("UPDATE ingredient SET name = '%s' WHERE id = '%s'", ingredient, id)
	return DbQuery(db, s)
}

func InsertIngredient(db *sql.DB, ingredient string) error {
	s := fmt.Sprintf("INSERT INTO ingredient(name) VALUES('%s')", ingredient)
	return DbQuery(db, s)
}

func DeleteIngredient(db *sql.DB, id string) error {
	s := fmt.Sprintf("DELETE FROM ingredient WHERE id = '%s'", id)
	return DbQuery(db, s)
}
