package database

import (
	"database/sql"
	"errors"
)

func DbQuery(db *sql.DB, query string) error {
	if db == nil {
		return errors.New("database not found")
	}
	_, err := db.Exec(query)
	if err != nil {
		return err
	}
	return nil
}
