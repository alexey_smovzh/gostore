package main

import (
	"gostore/database"
	"net/http"
	"strings"
)

// Display all products page
func productAll(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {
		product, err := database.GetStoreAllProducts(database.DB)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}

		m := map[string]interface{}{
			"Product": product,
			"Url":     "all",
		}

		err = templates["product"].ExecuteTemplate(w, "base", m)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}
	} else {
		http.Redirect(w, r, "/", http.StatusFound)
	}
}

// Display product full description page
func description(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {

		s := strings.Split(r.URL.Path, "/")
		id := s[len(s)-1]

		composition, err := database.GetComposition(database.DB, id)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}

		description, err := database.GetDescription(database.DB, id)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}

		media, err := database.GetMedia(database.DB, id)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}

		m := map[string]interface{}{
			"Description": description,
			"Composition": composition,
			"Media":       media,
			"Url":         "description",
		}

		err = templates["description"].ExecuteTemplate(w, "base", m)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}
	}
}
