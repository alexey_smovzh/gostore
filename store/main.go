package main

import (
	"gostore/settings"
	"net/http"
	"text/template"
)

var templates map[string]*template.Template

func main() {

	templates = make(map[string]*template.Template)
	templates["error"] = template.Must(template.ParseFiles("store/templates/error.html", "store/templates/base.html"))

	templates["product"] = template.Must(template.ParseFiles("store/templates/product.html", "store/templates/add_to_cart.html", "store/templates/base.html"))
	templates["description"] = template.Must(template.ParseFiles("store/templates/description.html", "store/templates/add_to_cart.html", "store/templates/base.html"))
	templates["cart"] = template.Must(template.ParseFiles("store/templates/cart.html", "store/templates/base.html"))
	templates["pay"] = template.Must(template.ParseFiles("store/templates/pay.html", "store/templates/base.html"))

	// Serve static files
	http.Handle("/css/", http.StripPrefix("/css/", http.FileServer(http.Dir("store/css"))))
	http.Handle("/js/", http.StripPrefix("/js/", http.FileServer(http.Dir("store/js"))))
	http.Handle("/media/", http.StripPrefix("/media/", http.FileServer(http.Dir(settings.MEDIA))))

	http.HandleFunc("/", productAll)
	http.HandleFunc("/description/", description)
	http.HandleFunc("/cart", cart)
	http.HandleFunc("/order", order)
	http.HandleFunc("/done", done)

	http.ListenAndServe(settings.LISTEN+":"+settings.PORT, nil)
}
