package main

import (
	"net/http"
)

func ErrorPage(w http.ResponseWriter, r *http.Request, message string) {
	m := map[string]interface{}{
		"Url":     "error",
		"Message": message,
	}
	err := templates["error"].ExecuteTemplate(w, "base", m)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
