package main

import (
	"encoding/json"
	"gostore/database"
	"net/http"
)

// Display shopping cart
func cart(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {
		m := map[string]interface{}{
			"Url": "cart",
		}
		err := templates["cart"].ExecuteTemplate(w, "base", m)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}
	}
}

type Order struct {
	Name  string      `json:"name"`
	Price int         `json:"price"`
	Qty   int         `json:"qty"`
	Sum   int         `json:"sum,omitempty"`
	Max   interface{} `json:"max"`
	Image string      `json:"thumb"`
}

// Parse JSON
func decode(source string) ([]*Order, int, error) {
	result := []*Order{}
	err := json.Unmarshal([]byte(source), &result)
	if err != nil {
		return nil, 0, err
	}
	// Calculate and set Sum on Product slice
	// Calculate order total sum
	total := 0
	for i := 0; i < len(result); i++ {
		result[i].Sum = result[i].Price * result[i].Qty
		total += result[i].Sum
	}

	return result, total, nil
}

// Get order data from cart.js
func order(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		order := r.FormValue("order")
		o, t, err := decode(order)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}

		m := map[string]interface{}{
			"Url":     "pay",
			"Product": o,
			"Total":   t,
		}

		err = templates["pay"].ExecuteTemplate(w, "base", m)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}
	}
}

// Final actions after customer complete purchase
func done(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		order := r.FormValue("order")

		o, _, err := decode(order)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}

		for i := 0; i < len(o); i++ {
			database.UpdateProductQty(database.DB, o[i].Name, o[i].Qty)
			if err != nil {
				ErrorPage(w, r, err.Error())
			}
		}

		// todo: send order details email to customer
		// todo: send order details email to admin

		http.Redirect(w, r, "/", http.StatusFound)
	}
}
