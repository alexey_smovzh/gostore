package media

import (
	"errors"
	"fmt"
	"gostore/database"
	"gostore/settings"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
)

// Create media folder if not exists
func createMediaFolder() error {
	_, err := os.Stat(settings.MEDIA)
	if os.IsNotExist(err) {
		err = os.MkdirAll(settings.MEDIA, 0700)
		if err != nil {
			return err
		}
	}
	return nil
}

func validate(file multipart.File) string {
	buffer := make([]byte, 512)
	_, err := file.Read(buffer)
	if err != nil {
		return err.Error()
	}

	return http.DetectContentType(buffer)
}

func Upload(r *http.Request, id string) error {
	err := r.ParseMultipartForm(32 << 20)
	if err != nil {
		return err
	}

	files := r.MultipartForm.File["images"]
	if len(files) > 0 {
		// todo: get from form '<img id=' and replace particular file

		// Delete media database records for this product to prevent dublication
		err = database.DeleteMedia(database.DB, id)
		if err != nil {
			return err
		}

		for idx, header := range files {
			if header.Size > settings.MAX_SIZE {
				return errors.New("File exeed 10Mb size limit")
			}

			file, err := header.Open()
			if err != nil {
				return err
			}
			defer file.Close()

			t := validate(file)
			if t != settings.FORMAT {
				return errors.New("File should be PNG image")
			}

			_, err = file.Seek(0, io.SeekStart)
			if err != nil {
				return err
			}

			n := fmt.Sprintf("%s_%d.png", id, idx)
			f, err := os.Create(settings.MEDIA + "/" + n)
			if err != nil {
				return err
			}
			defer f.Close()

			_, err = io.Copy(f, file)
			if err != nil {
				return err
			}

			err = database.InsertMedia(database.DB, id, n)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func Delete(id string) error {
	n := fmt.Sprintf("%s_*.png", id)
	files, err := filepath.Glob(settings.MEDIA + "/" + n)
	if err != nil {
		return err
	}

	for _, f := range files {
		err = os.Remove(f)
		if err != nil {
			return err
		}
	}
	return nil
}

func init() {
	err := createMediaFolder()
	if err != nil {
		println(err)
	}

}
