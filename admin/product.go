package main

import (
	database "gostore/database"
	media "gostore/media"
	"net/http"
	"regexp"
)

// Display all productss page
func productAll(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {
		product, err := database.GetAllProducts(database.DB)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}
		unit, err := database.GetAllUnits(database.DB)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}

		m := map[string]interface{}{
			"Unit":    unit,
			"Product": product,
		}

		err = templates["product"].ExecuteTemplate(w, "base", m)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}
	} else {
		http.Redirect(w, r, "/product/all", http.StatusFound)
	}
}

// Add new product (name, quantity, price and measurement unit)
func productAdd(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		product := r.FormValue("product")
		qty := r.FormValue("qty")
		price := r.FormValue("price")
		unit_id := r.FormValue("unit_id")

		valid, _ := regexp.MatchString("[А-Яа-я ]+$", product)
		if valid == false {
			ErrorPage(w, r, "Product name should consist only from cyrrilic letters")
		} else {
			err := database.InsertProduct(database.DB, product, qty, price, unit_id)
			if err != nil {
				ErrorPage(w, r, err.Error())
			}
			http.Redirect(w, r, "/product/all", http.StatusFound)
		}
	}
}

// Edit existing product
func productEdit(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {
		id := r.FormValue("id")
		product, err := database.GetProduct(database.DB, id)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}
		unit, err := database.GetAllUnits(database.DB)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}

		m := map[string]interface{}{
			"Unit":    unit,
			"Product": product,
		}

		err = templates["product"].ExecuteTemplate(w, "base", m)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}
	}
}

// Save product edit result
func productUpdate(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		id := r.FormValue("id")
		ingredient := r.FormValue("ingredient")
		valid, _ := regexp.MatchString("[А-Яа-я ]+$", ingredient)
		if valid == false {
			ErrorPage(w, r, "Ingredient name should consist only from cyrrilic letters")
		} else {
			err := database.UpdateIngredient(database.DB, ingredient, id)
			if err != nil {
				ErrorPage(w, r, err.Error())
			}

			http.Redirect(w, r, "/ingredient/all", http.StatusFound)
		}
	}
}

// Delete product
func productDelete(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		id := r.FormValue("id")
		err := database.DeleteProduct(database.DB, id)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}

		err = database.DeleteComposition(database.DB, id)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}

		err = database.DeleteMedia(database.DB, id)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}

		err = media.Delete(id)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}

		http.Redirect(w, r, "/product/all", http.StatusFound)
	}
}

// Display product full description page
func desciption(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {
		id := r.FormValue("id")
		product, err := database.GetDescription(database.DB, id)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}
		media, err := database.GetMedia(database.DB, id)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}

		m := map[string]interface{}{
			"Product": product,
			"Media":   media,
		}

		err = templates["description"].ExecuteTemplate(w, "base", m)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}
	}
}

// Save product full description
func desciptionUpdate(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		id := r.FormValue("id")
		laconic := r.FormValue("laconic")
		full := r.FormValue("full")

		err := database.UpdateDescription(database.DB, laconic, full, id)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}

		err = media.Upload(r, id)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}

		http.Redirect(w, r, "/product/all", http.StatusFound)
	}
}
