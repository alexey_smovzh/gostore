package main

import (
	"gostore/settings"
	"net/http"
	"text/template"
)

var templates map[string]*template.Template

func index(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/product/all", http.StatusFound)
}

func main() {

	templates = make(map[string]*template.Template)
	templates["error"] = template.Must(template.ParseFiles("admin/templates/error.html", "admin/templates/base.html"))

	templates["product"] = template.Must(template.ParseFiles("admin/templates/product.html", "admin/templates/base.html"))
	templates["composition"] = template.Must(template.ParseFiles("admin/templates/composition.html"))
	templates["description"] = template.Must(template.ParseFiles("admin/templates/description.html", "admin/templates/base.html"))
	templates["ingredient"] = template.Must(template.ParseFiles("admin/templates/ingredient.html", "admin/templates/base.html"))
	templates["unit"] = template.Must(template.ParseFiles("admin/templates/unit.html", "admin/templates/base.html"))

	// Serve static files
	http.Handle("/css/", http.StripPrefix("/css/", http.FileServer(http.Dir("admin/css"))))
	http.Handle("/media/", http.StripPrefix("/media/", http.FileServer(http.Dir(settings.MEDIA))))

	http.HandleFunc("/product/all", productAll)
	http.HandleFunc("/product/add", productAdd)
	http.HandleFunc("/product/edit", productEdit)
	http.HandleFunc("/product/update", productUpdate)
	http.HandleFunc("/product/delete", productDelete)
	http.HandleFunc("/product/description", desciption)
	http.HandleFunc("/product/descriptionupdate", desciptionUpdate)

	http.HandleFunc("/composition/", composition)
	http.HandleFunc("/composition/add", compositionAdd)
	http.HandleFunc("/composition/delete", compositionDelete)

	http.HandleFunc("/ingredient/all", ingredientAll)
	http.HandleFunc("/ingredient/add", ingredientAdd)
	http.HandleFunc("/ingredient/edit", ingredientEdit)
	http.HandleFunc("/ingredient/update", ingredientUpdate)
	http.HandleFunc("/ingredient/delete", ingredientDelete)

	http.HandleFunc("/unit/all", unitAll)
	http.HandleFunc("/unit/add", unitAdd)
	http.HandleFunc("/unit/delete", unitDelete)

	// Redirect to defaul page
	http.HandleFunc("/", index)
	http.ListenAndServe(settings.LISTEN+":"+settings.PORT, nil)
}
