package main

import (
	database "gostore/database"
	"net/http"
	"regexp"
)

// Display all units page
func unitAll(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {
		unit, err := database.GetAllUnits(database.DB)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}

		err = templates["unit"].ExecuteTemplate(w, "base", unit)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}
	} else {
		http.Redirect(w, r, "/unit/all", http.StatusFound)
	}
}

// Add new measurement unit
func unitAdd(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		unit := r.FormValue("unit")
		valid, _ := regexp.MatchString("[0-9]+[а-я]+$", unit)
		if valid == false {
			ErrorPage(w, r, "Measurement unit can contain only digits and cyrrilic letters in low case e.q. '1кг', '100мл'")
		} else {
			err := database.InsertUnit(database.DB, unit)
			if err != nil {
				ErrorPage(w, r, err.Error())
			}

			http.Redirect(w, r, "/unit/all", http.StatusFound)
		}
	}

}

// Delete measurement unit
func unitDelete(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		id := r.FormValue("id")
		err := database.DeleteUnit(database.DB, id)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}

		http.Redirect(w, r, "/unit/all", http.StatusFound)
	}

}
