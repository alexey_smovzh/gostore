package main

import (
	"net/http"
)

func ErrorPage(w http.ResponseWriter, r *http.Request, message string) {
	err := templates["error"].ExecuteTemplate(w, "base", message)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
