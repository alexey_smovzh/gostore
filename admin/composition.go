package main

import (
	"fmt"
	database "gostore/database"
	"net/http"
	"path"
	"regexp"
)

// Display composition for product by ID
// Last part of the URL is product ID
func composition(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {
		id := path.Base(r.URL.Path)

		composition, err := database.GetComposition(database.DB, id)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}

		ingredient, err := database.GetAllIngredients(database.DB)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}

		m := map[string]interface{}{
			"Composition": composition,
			"Ingredient":  ingredient,
			"Product_ID":  id,
		}

		err = templates["composition"].ExecuteTemplate(w, "base", m)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}
	}
}

// Add new ingredient in composition list
func compositionAdd(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		product_id := r.FormValue("product_id")
		ingredient_id := r.FormValue("ingredient_id")
		qty := r.FormValue("qty")

		valid, _ := regexp.MatchString("[0-9]+[а-я]+$", qty)
		if valid == false {
			ErrorPage(w, r, "Qty unit can contain only digits and cyrrilic letters in low case e.q. '20г', '1шт'")
		} else {
			err := database.InsertComposition(database.DB, product_id, ingredient_id, qty)
			if err != nil {
				ErrorPage(w, r, err.Error())
			}
			s := fmt.Sprintf("/composition/%s", product_id)
			http.Redirect(w, r, s, http.StatusFound)
		}
	}
}

// Delete composition ingredient
func compositionDelete(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		id := r.FormValue("id")
		product_id := r.FormValue("product_id")
		err := database.DeleteComposition(database.DB, id)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}
		s := fmt.Sprintf("/composition/%s", product_id)
		http.Redirect(w, r, s, http.StatusFound)
	}

}
