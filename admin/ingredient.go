package main

import (
	database "gostore/database"
	"net/http"
	"regexp"
)

// Display all ingredients page
func ingredientAll(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {
		ingredient, err := database.GetAllIngredients(database.DB)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}

		err = templates["ingredient"].ExecuteTemplate(w, "base", ingredient)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}
	} else {
		http.Redirect(w, r, "/ingredient/all", http.StatusFound)
	}
}

// Add new ingredient
func ingredientAdd(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		ingredient := r.FormValue("ingredient")
		valid, _ := regexp.MatchString("[А-Яа-я ]+$", ingredient)
		if valid == false {
			ErrorPage(w, r, "Ingredient name should consist only from cyrrilic letters")
		} else {
			err := database.InsertIngredient(database.DB, ingredient)
			if err != nil {
				ErrorPage(w, r, err.Error())
			}

			http.Redirect(w, r, "/ingredient/all", http.StatusFound)
		}
	}
}

// Edit existing ingredient
func ingredientEdit(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {
		id := r.FormValue("id")
		ingredient, err := database.GetIngredient(database.DB, id)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}

		err = templates["ingredient"].ExecuteTemplate(w, "base", ingredient)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}
	}
}

// Save ingredient edit result
func ingredientUpdate(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		id := r.FormValue("id")
		ingredient := r.FormValue("ingredient")
		valid, _ := regexp.MatchString("[А-Яа-я ]+$", ingredient)
		if valid == false {
			ErrorPage(w, r, "Ingredient name should consist only from cyrrilic letters")
		} else {
			err := database.UpdateIngredient(database.DB, ingredient, id)
			if err != nil {
				ErrorPage(w, r, err.Error())
			}

			http.Redirect(w, r, "/ingredient/all", http.StatusFound)
		}
	}
}

// Delete ingredient
func ingredientDelete(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		id := r.FormValue("id")
		err := database.DeleteIngredient(database.DB, id)
		if err != nil {
			ErrorPage(w, r, err.Error())
		}

		http.Redirect(w, r, "/ingredient/all", http.StatusFound)
	}
}
